package main

import (
	"api/routes/users"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	v1 := router.Group("/")
	users.User(v1)

	router.Run(":3001")
}
